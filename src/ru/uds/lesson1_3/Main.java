package ru.uds.lesson1_3;

/**
 * @author D. Utin 17IT18
 */
public class Main {
    public static void main(String[] args) {
        System.out.print(canBuy(15, 1, 3));
    }

    /**
     * Подсчитывает максимальное кол-во шоколадок
     *
     * @param money сумма в кармане
     * @param price стоимость шоколадки
     * @param wrap  обертки для получения бесплатной шоколадки
     * @return Максимальное кол-во шоколадок
     */
    private static int canBuy(int money, int price, int wrap) {
        int haveChocolates = 0; //Мои шоколадки
        for (int i = price, wrappers = 1; i <= money; i += price, wrappers++) {
            if (wrappers == wrap) {
                haveChocolates++;
                wrappers = 1;
            }
            haveChocolates++;
        }
        return haveChocolates;
    }
}
